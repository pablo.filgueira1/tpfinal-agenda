create database agenda;
use agenda;

CREATE TABLE `users` (
  `extension` varchar(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` VALUES ('450','Sergio Pernas','1139916450'),('451','Pablo Filgueira','1139916451'),('452','Roberto Sarasa','1139916452'),('453','Carlos Soria','1139916453'),('454','Maria Juana','1139916454');

CREATE USER 'consultaagenda'@'localhost' IDENTIFIED by 'pass';
GRANT SELECT ON agenda.* TO 'consultaagenda'@'localhost';

