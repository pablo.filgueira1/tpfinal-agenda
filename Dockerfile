FROM ubuntu:latest
RUN apt update
RUN apt install -y tzdata
RUN apt install -y mysql-server
RUN apt install -y apache2
RUN apt install -y php libapache2-mod-php php-mysql

ENV APACHE_RUN_DIR="/var/www/html"
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

RUN rm -r /var/www/html
COPY web /var/www/html
RUN chown -R www-data:www-data /var/www

COPY iniciardb.sql /
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
