# TpFinal-Agenda

Proyecto de agenda con base de datos en mysql en docker
```
Nombre y Apellido: Pablo Filgueira
Materia: Infraestructura de Servidores
Profesor: Sergio Pernas
```

## Pasos para deployar el proyecto
_**Clonar el repo localmente**_

`git clone https://gitlab.com/pablo.filgueira1/tpfinal-agenda.git`


_**Hacer el buil de la imagen a partir del Dockerfile**_

`docker build -t agenda tpfinal-agenda/`
> - '-t' Nombre imagen
> - 'tpfinal-agenda/' Carpeta donde esta el Dockerfile


_**Lanzar un contenedor a partir de la imagen creada**_

`docker run --name agenda -d -p 8080:80 agenda` 
> - '--name' Seteo de nombre para el contenedor
> - '-d' Ejecuta los procesos en modo 'daemon'
> - '-p' Mapeo de puertos
> - 'agenda' Nombre de la imagen a utilizar



## Explicacion Dockerfile
> FROM indica una imagen base para crear una nueva imagen
```
FROM ubuntu:latest
```

> RUN ejecuta comandos dentro del contenedor. Instalamos todo lo necesario para que funcione la pagina web (apache, php y mysql)
```
RUN apt update
RUN apt install -y tzdata
RUN apt install -y mysql-server
RUN apt install -y apache2
RUN apt install -y php libapache2-mod-php php-mysql
```

> ENV variables de entorno que necesita apache para levantar correctamente.
```
ENV APACHE_RUN_DIR="/var/www/html"
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
```

> Borramos la carpeta /var/www/html dentro del cotenedor 
```
RUN rm -r /var/www/html
```

> Copiamos la carpeta web a /var/www/html dentro del cotenedor
```
COPY web /var/www/html
```

> Le damos permiso al usuario www-data sobre la carpeta  /var/www
```
RUN chown -R www-data:www-data /var/www
```

> Copiamos los archivos iniciardb.sql y entrypoint.sh a / dentro del contenedor
```
COPY iniciardb.sql /
COPY entrypoint.sh / 
```

> Damos permiso de ejecucion al archivo entrypoint.sh
```
RUN chmod +x /entrypoint.sh
```

> ENTRYPOINT indica que script se ejecutara cuando se lanze el contenedor
```
ENTRYPOINT ["/entrypoint.sh"]
```

> Exponemos el puerto 80
```
EXPOSE 80
```

> Iniciamos el servicio apache
```
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
```



## Explicacion de entrypoint.sh
> Iniciamos el servicio mysql
```
#!/bin/bash
service mysql start
```

> Ejecutamos el script iniciardb.sql dentro de mysql, el script tiene todo lo necesario para crear la base de datos e insertar los datos necesarios.
```
mysql < /iniciardb.sql
```

> A este fichero se la van a pasar las intrucciones que vengan de 'CMD'
```
exec "$@"
```



## Explicacion de iniciardb.sql
> Creamos la base de datos agenda
```
create database agenda;
```
>Creamos la tabla users
```
use agenda;
CREATE TABLE `users` (
  `extension` varchar(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```

> Insertamos los datos dentro de la tabla users
```
INSERT INTO `users` VALUES ('450','Sergio Pernas','1139916450'),('451','Pablo Filgueira','1139916451'),('452','Roberto Sarasa','1139916452'),('453','Carlos Soria','1139916453'),('454','Maria Juana','1139916454');
```

> Creamos el usuario consultaagenda el cual va a usar el aplicativo web para consultar la base de datos agenda y le damos permiso para dicha tarea
```
CREATE USER 'consultaagenda'@'localhost' IDENTIFIED by 'pass';
GRANT SELECT ON agenda.* TO 'consultaagenda'@'localhost';
```


