<!DOCTYPE html>
<html>
	<head>
		<title>Agenda Telefonica Banco Saenz</title>

		<!-- Estilos hechos en CSS -->
		<link rel="stylesheet" type="text/css" href="./estilos.css"> 
		<script type="text/javascript" src="./buscar.js"></script>
		
		<!-- estilo banner -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
		body, html {
		  height: 100%;
		  margin: 0;
		  font-family: Arial, Helvetica, sans-serif;
		}

		.hero-image {
		  background-image: url("logo.png");
		  height: 10%;
		  background-position: center;
		  background-repeat: no-repeat;
		  background-size: cover;
		  position: relative;
		}
		</style>
	
	</head>
	<body style="background-color:#E1EEF4;">
	<div class="hero-image">
		<div class="hero-text">
		</div>
	</div>


	<!-- Y acá la tabla que irá levantando, según la página, la info pertinente dentro de la DB -->
			<table id="table2">
				<tr>
					<th>Extension</th>
					<th>Nombre</th>
					<th>Telefono</th>
				</tr>
				<tr>
					<!-- estos ítems corresponden al buscador. Más abajo está la función por cada uno de ellos -->
					<th><input type="text" id="busca_ex" onkeyup="buscar_ex()" placeholder="buscar..."></th>
					<th><input type="text" id="busca_nombre" onkeyup="buscar_nombre()" placeholder="buscar..."></th>
					<th><input type="text" id="busca_numero" onkeyup="buscar_numero()" placeholder="buscar..."></th>
				</tr>

	<!-- Acá arranco, en PHP, con la consulta a la DB -->
				<?php
					include "./conexion.php";
					include "./tabla.php";
				?>
			</table>
	</body>
</html>

<!--FIN-->
