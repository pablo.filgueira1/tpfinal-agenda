function buscar_ex() {
/* Declaro Variables */
var input, filter, table, tr, td, i, txtValue;
input = document.getElementById("busca_ex");
filter = input.value.toUpperCase();
table = document.getElementById("table2");
tr = table.getElementsByTagName("tr");

/* Hago un loop por las filas de la tabla encontrando lo que quiero y ocultando lo que no quiero */
for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0]; /* El número del final indica qué posición. En este caso 0 para ID, 1 para nombre y 2 para símbolo */
    if (td) {
    txtValue = td.textContent || td.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
    } else {
        tr[i].style.display = "none";
    }
    } 
}
}

function buscar_nombre() {
/* Declaro Variables */
var input, filter, table, tr, td, i, txtValue;
input = document.getElementById("busca_nombre");
filter = input.value.toUpperCase();
table = document.getElementById("table2");
tr = table.getElementsByTagName("tr");

/* Hago un loop por las filas de la tabla encontrando lo que quiero y ocultando lo que no quiero */
for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
    txtValue = td.textContent || td.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
    } else {
        tr[i].style.display = "none";
    }
    } 
}
}

function buscar_numero() {
/* Declaro Variables */
var input, filter, table, tr, td, i, txtValue;
input = document.getElementById("busca_numero");
filter = input.value.toUpperCase();
table = document.getElementById("table2");
tr = table.getElementsByTagName("tr");

/* Hago un loop por las filas de la tabla encontrando lo que quiero y ocultando lo que no quiero */
for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
    txtValue = td.textContent || td.innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
    } else {
        tr[i].style.display = "none";
    }
    } 
}
}